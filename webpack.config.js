const path = require('path');

module.exports = {
    mode: process.env.NODE_ENV,
    entry: {
        app: path.resolve(__dirname, 'src/js/app.js'),
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'build/js/script.js')
    },
    devtool: process.env.NODE_ENV === 'development' ? 'source-map' : false,
    devServer: {
        contentBase: path.join(__dirname, 'build'),
    	// port: 8080,
    	hot: true
    },
    module: {
        rules: [{
            test: /\.js$/,
            loader: 'babel-loader',
            exclude: '/node_modules/'
        }, ]
    },
    resolve: {
        extensions: ['.js', '.json'],
        alias: {
            '@': path.resolve(__dirname, 'src'),
            '@utils': path.resolve(__dirname, 'src/assets/scripts/utils/'),
        }
    },
    devServer: {
        overlay: false
    }
}